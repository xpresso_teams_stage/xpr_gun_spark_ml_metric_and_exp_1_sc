
import os

from pyspark.ml.feature import OneHotEncoderEstimator
from .abstract_component import AbstractPipelineEstimator

class CustomerOneHotEncoderEstimator(OneHotEncoderEstimator, AbstractPipelineEstimator):

    def __init__(self, name, run_id, inputCols=None, outputCols=None, handleInvalid='error', dropLast=True):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} with run_id {run_id}')
        OneHotEncoderEstimator.__init__(self, inputCols=inputCols, outputCols=outputCols, handleInvalid=handleInvalid, dropLast=dropLast)
        AbstractPipelineEstimator.__init__(self, run_id)

    def _fit(self, dataset):
        print("In CustomerOneHotEncoderEstimator _fit, now calling super's _fit")
        args = [self.run_id]
        self.start(*args)
        return super()._fit(dataset)