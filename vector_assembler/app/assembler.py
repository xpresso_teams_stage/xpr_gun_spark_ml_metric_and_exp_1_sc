
import os

from pyspark.ml.feature import VectorAssembler
from .abstract_component import AbstractPipelineTransformer

class CustomVectorAssembler(VectorAssembler, AbstractPipelineTransformer): 

    def __init__(self, name, run_id, inputCols=None, outputCol=None, handleInvalid='error'):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} with run_id {run_id}')
        VectorAssembler.__init__(self, inputCols=inputCols, outputCol=outputCol, handleInvalid=handleInvalid)
        AbstractPipelineTransformer.__init__(self, run_id)
    
    def _transform(self, dataset):
        print("In CustomVectorAssembler _transform, now calling super's _transform")
        args = [self.run_id]
        self.start(*args)
        return super()._transform(dataset)