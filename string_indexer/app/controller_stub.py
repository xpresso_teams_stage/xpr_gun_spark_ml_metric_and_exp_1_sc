from datetime import datetime
import pickle
import os
import requests


class ControllerStub:

    def __init__(self):
        pass

    def pipeline_component_started(self, run_id, component_name):
        self.report_pipeline_status(run_id, component_name,
                                    {"status": {"status": "component started"}})

    def pipeline_component_terminated(self, run_id, component_name):
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component terminated"}})

    def pipeline_component_paused(self, run_id, component_name,
                                  component_state):
        # delete any state file
        self.clean_state(run_id, component_name)
        self.save_state(run_id, component_name, component_state)
        self.report_pipeline_status(run_id, component_name,
                                    {"status": {"status": "component paused"}})

    def pipeline_component_restarted(self, run_id, component_name):
        component_state = self.load_state(run_id, component_name)
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component restarted"}})
        return component_state

    def pipeline_component_completed(self, run_id, component_name):
        # delete any state file
        self.clean_state(run_id, component_name)
        self.report_pipeline_status(run_id, component_name, {
            "status": {"status": "component completed"}})

    def get_pipeline_run_status(self, run_id):
        f = open("/data/run_status_" + str(run_id) + ".txt", "r")
        status = f.readline().rstrip('\n')
        f.close()
        return status

    def update_pipeline_run_status(self, run_id, status):
        f = open("/data/run_status_" + str(run_id) + ".txt", "w")
        f.write(status)
        f.close()

    def report_pipeline_status(self, run_id, component_name, status):
        now = datetime.now()
        curr_time = now.strftime("%Y-%m-%d %H:%M:%S")
        print(
            "Run ID: {}, Time:{}, Component: {}, Status: {}".format(str(run_id),
                                                                    curr_time,
                                                                    component_name,
                                                                    status),
            flush=True)

    def save_state(self, run_id, component_name, state):
        print("Saving state", flush=True)
        print(state, flush=True)
        state.save('/user/root/fenml.out')
        pickle_out = open(self.get_state_filename(run_id, component_name), "wb")
        pickle.dump(state, pickle_out)
        pickle_out.close()

    def load_state(self, run_id, component_name):
        print("Loading state", flush=True)
        state_file = self.get_state_filename(run_id, component_name)
        state = None
        if os.path.exists(state_file):
            pickle_in = open(state_file, "rb")
            state = pickle.load(pickle_in)
            pickle_in.close()
        print(state, flush=True)
        return state

    def get_state_filename(self, run_id, component_name):
        return "/data/" + str(run_id) + "_" + component_name + ".pkl"

    def clean_state(self, run_id, component_name):
        state_file = self.get_state_filename(run_id, component_name)
        if os.path.exists(state_file):
            os.remove(state_file)
