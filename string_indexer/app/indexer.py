
import os

from pyspark.ml.feature import StringIndexer
from .abstract_component import AbstractPipelineEstimator

class CustomStringIndexer(StringIndexer, AbstractPipelineEstimator):
    
    def __init__(self, name, run_id, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} with run_id {run_id}')
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractPipelineEstimator.__init__(self, run_id)

    def _fit(self, dataset):
        print("In CustomStringIndexer _fit, now calling super's _fit")
        args = [self.run_id]
        self.start(*args)
        return super()._fit(dataset)

class LabelIndexer(StringIndexer, AbstractPipelineEstimator):
    
    def __init__(self, name, run_id, inputCol=None, outputCol=None, handleInvalid='error', stringOrderType='frequencyDesc'):
        self.name = name
        class_name = self.__class__.__name__
        print(f'In class: {class_name} with run_id {run_id}')
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol, handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractPipelineEstimator.__init__(self, run_id)

    def _fit(self, dataset):
        print("In LabelIndexer _fit, now calling super's _fit")
        args = [self.run_id]
        self.start(*args)
        return super()._fit(dataset)